import sys

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QApplication

from nnrss_qt.main_window import MainWindow

app = QApplication([])


def run_main():
    QCoreApplication.setApplicationName("nnrss_qt")
    QCoreApplication.setOrganizationName("nnrss_qt")
    application = MainWindow()
    application.show()

    app.exec()


if __name__ == "__main__":
    sys.exit(run_main())
