from PyQt5.QtCore import QSettings

from nnrss_qt.default_values import DefaultValues


class Settings:
    def __init__(self):
        settings = QSettings()
        def_val = DefaultValues()

        self.first_run = settings.value("first_run", True, bool)
        self.domain = settings.value("domain", def_val.default_domain, str)
        self.username = settings.value("username", def_val.default_creds, str)
        self.api_key = settings.value("api_key", def_val.default_creds, str)
        self.update_interval_minutes = settings.value(
            "update_interval", def_val.default_update_time, int
        )
        self.only_unread_str = settings.value(
            "feed_filter", def_val.all_feeds_filter, str
        )

        if self.only_unread_str == def_val.all_feeds_filter:
            self.only_unread_settings = False
        else:
            self.only_unread_settings = True
