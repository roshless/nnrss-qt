from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QDialog

from nnrss_qt.api_handler import ApiHandler


class DialogAddFeed(QDialog):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 130)
        Dialog.setMinimumSize(QtCore.QSize(400, 130))
        Dialog.setMaximumSize(QtCore.QSize(400, 130))
        self.formLayout = QtWidgets.QFormLayout(Dialog)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok
        )
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.buttonBox)
        self.lineEditURL = QtWidgets.QLineEdit(Dialog)
        self.lineEditURL.setObjectName("lineEditURL")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditURL)
        self.lineEditCategory = QtWidgets.QLineEdit(Dialog)
        self.lineEditCategory.setMaxLength(25)
        self.lineEditCategory.setObjectName("lineEditCategory")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.lineEditCategory
        )
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Link to RSS feed"))
        self.label_2.setText(_translate("Dialog", "Category name"))

    def __init__(self, request: ApiHandler):
        self.r = request

        super(DialogAddFeed, self).__init__()
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.add_feed)

        self.buttonBox.setEnabled(False)
        self.lineEditURL.textChanged.connect(self.text_changed)
        self.lineEditCategory.textChanged.connect(self.text_changed)

    def text_changed(self):
        input_url = self.lineEditURL.text()
        input_category = self.lineEditCategory.text()
        if input_url == "" or input_category == "":
            self.buttonBox.setEnabled(False)
        else:
            self.buttonBox.setEnabled(True)

    def add_feed(self):
        input_url = self.lineEditURL.text()
        input_category = self.lineEditCategory.text()
        self.r.add_feed(input_url, input_category)
