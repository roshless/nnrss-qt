from subprocess import run
from webbrowser import open

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (
    QListWidgetItem,
    QMainWindow,
    QMessageBox,
    QSizePolicy,
    QWidget,
)

from nnrss_qt.add_dialog import DialogAddFeed
from nnrss_qt.api_handler import ApiHandler
from nnrss_qt.feed_widget import QWidgetFeeds
from nnrss_qt.settings import Settings
from nnrss_qt.settings_window import SettingsWindow
from nnrss_qt.showall_widget import QShowAllSwitch
from nnrss_qt.thread import (
    LoadAllEntriesThread,
    LoadEntriesThread,
    LoadFeedsThread,
    LoopUpdateFeedsThread,
)
from nnrss_qt.wizard import RssWizard


class MainWindow(QMainWindow):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.NonModal)
        MainWindow.setEnabled(True)
        MainWindow.resize(840, 682)
        MainWindow.setMinimumSize(QtCore.QSize(400, 250))
        icon = QtGui.QIcon.fromTheme("GTK3")
        MainWindow.setWindowIcon(icon)
        MainWindow.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.listWidgetFeeds = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.listWidgetFeeds.sizePolicy().hasHeightForWidth()
        )
        self.listWidgetFeeds.setSizePolicy(sizePolicy)
        self.listWidgetFeeds.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.listWidgetFeeds.setObjectName("listWidgetFeeds")
        self.horizontalLayout_2.addWidget(self.listWidgetFeeds)
        self.listWidgetEntries = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.listWidgetEntries.sizePolicy().hasHeightForWidth()
        )
        self.listWidgetEntries.setSizePolicy(sizePolicy)
        self.listWidgetEntries.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.listWidgetEntries.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContentsOnFirstShow
        )
        self.listWidgetEntries.setObjectName("listWidgetEntries")
        self.horizontalLayout_2.addWidget(self.listWidgetEntries)
        MainWindow.setCentralWidget(self.centralwidget)
        self.toolBar = QtWidgets.QToolBar(MainWindow)
        self.toolBar.setMovable(False)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAdd = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("list-add")
        self.actionAdd.setIcon(icon)
        self.actionAdd.setObjectName("actionAdd")
        self.actionDelete = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("list-remove")
        self.actionDelete.setIcon(icon)
        self.actionDelete.setObjectName("actionDelete")
        self.actionSettings = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("preferences-system")
        self.actionSettings.setIcon(icon)
        self.actionSettings.setObjectName("actionSettings")
        self.actionRefresh = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("view-refresh")
        self.actionRefresh.setIcon(icon)
        self.actionRefresh.setObjectName("actionRefresh")
        self.actionQuit = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("application-exit")
        self.actionQuit.setIcon(icon)
        self.actionQuit.setObjectName("actionQuit")
        self.actionAbout = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("help-about")
        self.actionAbout.setIcon(icon)
        self.actionAbout.setObjectName("actionAbout")
        self.actionShowAll = QtWidgets.QAction(MainWindow)
        self.actionShowAll.setCheckable(True)
        icon = QtGui.QIcon.fromTheme("preferences-system")
        self.actionShowAll.setIcon(icon)
        self.actionShowAll.setObjectName("actionShowAll")
        self.toolBar.addAction(self.actionAdd)
        self.toolBar.addAction(self.actionDelete)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionRefresh)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionSettings)
        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addSeparator()

        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.toolBar.addWidget(spacer)

        self.showall_switch = QShowAllSwitch()
        self.toolBar.addWidget(self.showall_switch)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "nano nano rss reader"))
        self.toolBar.setWindowTitle(_translate("MainWindow", "tool bar"))
        self.actionAdd.setText(_translate("MainWindow", "Add"))
        self.actionDelete.setText(_translate("MainWindow", "Delete"))
        self.actionSettings.setText(_translate("MainWindow", "Settings"))
        self.actionRefresh.setText(_translate("MainWindow", "Refresh"))
        self.actionQuit.setText(_translate("MainWindow", "Quit"))
        self.actionAbout.setText(_translate("MainWindow", "About"))
        self.actionShowAll.setText(_translate("MainWindow", "Show all feeds"))

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.settings = Settings()

        if self.settings.first_run:
            self.wizard = RssWizard()
            self.wizard.show()

        self.api_handler = ApiHandler(
            self.settings.domain,
            self.settings.username,
            self.settings.api_key,
            self.settings.update_interval_minutes,
        )

        self.init_feeds_thread()
        self.init_loop_thread(self.settings.update_interval_minutes)

        self.showall_switch.show_all_clicked.connect(self.feed_selected_finish)
        self.showall_switch.show_unread_clicked.connect(
            lambda: self.feed_selected_finish(only_unread=True)
        )
        self.showall_switch.set_state(self.settings.only_unread_settings)

        self.listWidgetFeeds.itemSelectionChanged.connect(
            lambda: self.feed_selected_start(
                only_unread=self.showall_switch.is_unread_checked()
            )
        )
        self.listWidgetEntries.doubleClicked.connect(self.open_url)

        self.actionSettings.triggered.connect(self.run_settings)
        self.actionQuit.triggered.connect(self.close)
        self.actionRefresh.triggered.connect(self.init_feeds_thread)
        self.actionDelete.triggered.connect(self.delete_feed)
        self.actionAdd.triggered.connect(self.add_feed)

    def init_feeds_thread(self):
        self.feeds_thread = LoadFeedsThread(self.api_handler)
        self.feeds_thread.finished_signal.connect(self.connect_nnrss)
        self.feeds_thread.start()

    def init_loop_thread(self, update_interval_minutes):
        self.loop_thread = LoopUpdateFeedsThread(update_interval_minutes)
        self.loop_thread.update_now.connect(self.update_feeds_start)
        self.loop_thread.start()

    def connect_nnrss(self, result: ApiHandler = None, force=False):
        if result:
            self.api_handler = result
        self.listWidgetFeeds.clear()
        for _, feed_object in self.api_handler.get_feeds(force):
            feed_widget = QWidgetFeeds(self)
            feed_widget.disableIcon()
            feed_widget.setUpLabel(feed_object.title)
            feed_widget.setCategoryLabel(feed_object.category)
            feed_widget.setDownLabel(feed_object.url)

            myQListWidgetItem = QListWidgetItem(self.listWidgetFeeds)
            myQListWidgetItem.setSizeHint(feed_widget.sizeHint())

            self.listWidgetFeeds.addItem(myQListWidgetItem)
            self.listWidgetFeeds.setItemWidget(myQListWidgetItem, feed_widget)

        self.statusBar().showMessage(self.api_handler.get_latest_status())

    def run_settings(self):
        self.settings_win = SettingsWindow()
        self.settings_win.show()

    def feed_selected_start(self, only_unread=False):
        index = self.listWidgetFeeds.currentRow()

        self.entries_thread = LoadEntriesThread(self.api_handler, index, only_unread)
        self.entries_thread.finished_signal.connect(self.feed_selected_finish)
        self.entries_thread.start()

    def feed_selected_finish(self, result: ApiHandler = None, only_unread=False):
        if result:
            self.api_handler = result
        item = self.listWidgetFeeds.selectedItems()
        self.listWidgetEntries.clear()
        if len(item) != 0:
            index = self.listWidgetFeeds.currentRow()
            for element_index, element in enumerate(
                self.api_handler.get_entries(index, only_unread), 1
            ):
                feed_widget = QWidgetFeeds()
                feed_widget.setUpLabel(element.title)
                feed_widget.setDownLabel(element.date)

                entryWidgetItem = QListWidgetItem(self.listWidgetEntries)
                if not only_unread and element.unread:
                    entryWidgetItem.setBackground(QColor("lightblue"))
                entryWidgetItem.setSizeHint(feed_widget.sizeHint())

                self.listWidgetEntries.addItem(entryWidgetItem)
                self.listWidgetEntries.setItemWidget(entryWidgetItem, feed_widget)
        self.statusBar().showMessage(self.api_handler.get_latest_status())

    def open_url(self):
        index_feed = self.listWidgetFeeds.currentRow()
        index_entries = self.listWidgetEntries.currentRow()
        url = self.api_handler.get_link(index_feed, index_entries)
        open(url)
        self.api_handler.set_read(index_feed, index_entries)
        self.listWidgetEntries.item(index_entries).setBackground(QColor("lightblue"))

    def eventFilter(self, widget, event):
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Space:
                self.open_url()
                return True
            return False
        return QtWidgets.QWidget.eventFilter(self, widget, event)

    def add_feed(self):
        self.dialog = DialogAddFeed(self.api_handler)
        self.dialog.show()
        self.dialog.exec_()
        status = self.api_handler.get_latest_status()
        if status == "Feed added":
            self.connect_nnrss(force=True)
        self.statusBar().showMessage(status)

    def delete_feed(self):
        quit_msg = "Are you sure you want to delete this feed?"
        reply = QMessageBox.question(
            self, "Message", quit_msg, QMessageBox.Yes, QMessageBox.No
        )
        if reply == QMessageBox.Yes:
            items = self.listWidgetFeeds.selectedItems()
            if len(items) != 0:
                index = self.listWidgetFeeds.currentRow()
                if self.api_handler.remove_feed(index):
                    self.connect_nnrss(force=True)
                self.statusBar().showMessage(self.api_handler.get_latest_status())

    def update_feeds_start(self):
        self.load_all_entries_thread = LoadAllEntriesThread(self.api_handler)
        self.load_all_entries_thread.finished_signal.connect(self.update_feeds_finish)
        self.load_all_entries_thread.start()

    def update_feeds_finish(self, updated_entries_count: int):
        if updated_entries_count > 0:
            title = "Feeds updated"
            body = "{} new entries".format(str(updated_entries_count))
            run(
                ["notify-send", title, body, "--icon=/usr/share/nnrss_qt/feed_icon.svg"]
            )

    def closeEvent(self, event):
        self.api_handler.save_cached()
