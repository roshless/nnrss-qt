from time import sleep

from PyQt5.QtCore import QThread, pyqtSignal

from nnrss_qt.api_handler import ApiHandler


class LoadFeedsThread(QThread):
    finished_signal = pyqtSignal(ApiHandler)

    def __init__(self, req: ApiHandler):
        QThread.__init__(self)
        self.api_handler = req

    def run(self):
        self.api_handler.get_feeds(False)
        self.finished_signal.emit(self.api_handler)


class LoadEntriesThread(QThread):
    finished_signal = pyqtSignal(ApiHandler, bool)

    def __init__(self, req: ApiHandler, index: int, only_unread: bool):
        QThread.__init__(self)
        self.api_handler = req
        self.index = index
        self.only_unread = only_unread

    def run(self):
        self.api_handler.get_entries(self.index)
        self.finished_signal.emit(self.api_handler, self.only_unread)


class LoopUpdateFeedsThread(QThread):
    update_now = pyqtSignal()

    def __init__(self, update_interval_minutes: int):
        QThread.__init__(self)
        self.update_interval_seconds = update_interval_minutes * 60

    def run(self):
        while True:
            sleep(self.update_interval_seconds)
            self.update_now.emit()


class LoadAllEntriesThread(QThread):
    finished_signal = pyqtSignal(int)

    def __init__(self, req: ApiHandler):
        QThread.__init__(self)
        self.api_handler = req

    def run(self):
        feeds_updated: int = self.api_handler.get_all_entries()
        self.finished_signal.emit(feeds_updated)
