import io

from setuptools import setup

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name='nnrss-qt',
    author='Roshless',
    author_email='code@roshless.com',
    version='0.1',
    packages=['nnrss_qt'],
    long_description=readme,
    include_package_data=True,
    entry_points={'gui_scripts': ['nnrss-qt = nnrss_qt.__main__:run_main']},
    install_requires=[
        'PyQt5',
        'requests',
    ],
    python_requires='>=3.5',
)
