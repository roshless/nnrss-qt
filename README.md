# nnrss-qt

### Qt GUI for nnrss


Features:

- fast
- displaying all feed entries or just unread
- updates in background


#### Packages
Available in [AUR](https://aur.archlinux.org/packages/nnrss-qt/)


#### Screenshots

[See wiki](https://man.roshless.me/~roshless/nnrss-qt)
